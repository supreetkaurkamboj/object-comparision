/* The following java program demonstrates how to compare the state of two objects without using comparator */

import java.util.*;


class myException extends Exception
{
  public myException(String s)
   {
     super(s);
   }
}
public class Equal extends abc
  {

    
    public static void main(String[] args) 
     {

       try                        //try block for handling exceptions
        {
          Scanner sc = new Scanner(System.in);

           //Taking input from user.
          System.out.println("Enter details of 1st student: ");

          System.out.println("Enter name:");

          String name=sc.next();

          System.out.println("Enter age:");

          int age=sc.nextInt();

          if(age>20)
            {
               throw new myException("Incorrect"); 
            }
          System.out.println("Enter rollno:");

          int roll=sc.nextInt();

          System.out.println("Enter details of 2nd student: ");

          System.out.println("Enter name:");

          String name1=sc.next();

          System.out.println("Enter age:");

          int age1=sc.nextInt();
          if(age1>20)
           {
             throw new myException("Incorrect"); 
           }

          System.out.println("Enter rollno:");

          int roll1=sc.nextInt();
   
          Students s = new Students(name,age,roll)
;       // create object of students class
          Students s1 = new Students(name1,age1,roll1);  
          System.out.println("Is both objects are same:? "+s.equals(s1));
  
        }
      catch(myException ex)             //Handle the exception for age
        {
           System.out.println("Age should be in the range of 1 to 20 range");
        }

  }

}
class Students 
{

        
String name;
        
int age;
        
int rollno;

        
public Students(String name, int age, int rollno) 
  {
            
    this.name = name;
            
    this.age = age;
            
    this.rollno = rollno;
        
  }

        
public boolean equals(Object o) 
  {
         
            
    if (o == null || getClass() != o.getClass()) 
     {
                
       return false;
            
     }
            
    Students s1 = (Students) o;
            
    return age == s1.age &&
 rollno == s1.rollno &&
 name.equals(s1.name);
        
  }
} 

